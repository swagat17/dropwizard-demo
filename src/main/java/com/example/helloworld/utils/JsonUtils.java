package com.example.helloworld.utils;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.example.helloworld.exception.DemoException;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

public class JsonUtils {
	private static final Logger log = Logger.getLogger(JsonUtils.class.getName());
	private static Gson gson = new Gson();

	public static String getMessageJson(String message) {
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("message", message);
		return gson.toJson(jsonObject);
	}

	public static String getJson(Object object) {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.registerModule(new Hibernate5Module()).setSerializationInclusion(Include.NON_NULL);
		objectMapper.setVisibility(PropertyAccessor.ALL, Visibility.ANY);
		String json = null;
		try {
			json = objectMapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			log.log(Level.WARNING, e.getMessage());
		}
		return json;
	}

	public static String getJson(JsonObject jsonObject) {
		return gson.toJson(jsonObject);
	}

	public static void validateJson(String json) throws DemoException {
		try {
			gson.fromJson(json, Object.class);
		} catch (JsonSyntaxException ex) {
			throw new DemoException(ex.getMessage());
		}

	}
}