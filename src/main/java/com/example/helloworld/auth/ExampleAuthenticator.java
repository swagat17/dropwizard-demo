package com.example.helloworld.auth;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.helloworld.core.User;
import com.example.helloworld.exception.DemoException;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;
import io.dropwizard.auth.basic.BasicCredentials;

public class ExampleAuthenticator implements Authenticator<BasicCredentials, User> {
	private static final Logger LOGGER = LoggerFactory.getLogger(ExampleAuthenticator.class);
	private AuthenticatorDAOProxy authenticatorDAOProxy;
   
	public ExampleAuthenticator(AuthenticatorDAOProxy authenticatorDAOProxy) {
		this.authenticatorDAOProxy = authenticatorDAOProxy;
	}
	
	/**
     * Valid users with mapping user -> roles
     */
    private static final Map<String, Set<String>> VALID_USERS = ImmutableMap.of(
        "guest", ImmutableSet.of(),
        "good-guy", ImmutableSet.of("BASIC_GUY"),
        "chief-wizard", ImmutableSet.of("ADMIN", "BASIC_GUY")
    );

    @Override
	public Optional<User> authenticate(BasicCredentials credentials) throws AuthenticationException {
		if (VALID_USERS.containsKey(credentials.getUsername()) && "secret".equals(credentials.getPassword())) {
			return Optional.of(new User(credentials.getUsername(), VALID_USERS.get(credentials.getUsername())));
		}

		try {
			String username = credentials.getUsername();
			String password = credentials.getPassword();

			Boolean isValidPerson = authenticatorDAOProxy.isValidPerson(username, password);
			if (isValidPerson == false) {
				throw new DemoException("incorrect credentials");
			}
			return Optional.of(new User(username, ImmutableSet.of()));
		} catch (Exception e) {
			LOGGER.error("Failed to authorize user. Reason: " + e.getMessage());
		}
		throw new WebApplicationException(Status.UNAUTHORIZED);
	}
}
