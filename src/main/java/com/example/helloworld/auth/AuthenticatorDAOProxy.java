package com.example.helloworld.auth;

import com.example.helloworld.core.Person;
import com.example.helloworld.db.PersonDAO;

import io.dropwizard.hibernate.UnitOfWork;

public class AuthenticatorDAOProxy {

	private PersonDAO personDAO;

	public AuthenticatorDAOProxy(PersonDAO personDAO) {
		this.personDAO = personDAO;
	}

	@UnitOfWork
	public boolean isValidPerson(String username, String password) {
		Person person = personDAO.login(username, password);
		if (person != null) {
			return true;
		}
		return false;
	}

}
