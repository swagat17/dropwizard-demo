package com.example.helloworld.resources;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.helloworld.core.Person;
import com.example.helloworld.core.User;
import com.example.helloworld.db.PersonDAO;
import com.example.helloworld.exception.DemoException;
import com.example.helloworld.resources.dto.PersonDTO;
import com.example.helloworld.utils.JsonUtils;

import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;

@Path("/demo")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DemoResource {
	private static final Logger LOGGER = LoggerFactory.getLogger(DemoResource.class);
	private PersonDAO personDAO;

	public DemoResource(PersonDAO personDAO) {
		this.personDAO = personDAO;
	}

	@POST
	@UnitOfWork
	@Path("/save")
	public Response savePerson(@NotNull(message = "Request cannot be empty") 
		@Valid PersonDTO personDTO) {
		try {
			Person person = personDAO.findByUserName(personDTO.getUsername());
			if (person != null) {
				throw new DemoException("Username already taken");
			}
		} catch (Exception ex) {
			LOGGER.error("Failed to validate create person. Reason: " + ex.getMessage());
			throw new WebApplicationException(
					Response.status(Status.BAD_REQUEST).entity(JsonUtils.getMessageJson(ex.getMessage())).build());
		}

		try {
			Person person = new Person();
			person.setFullName(personDTO.getFullName());
			person.setJobTitle(personDTO.getJobTitle());
			person.setPassword(personDTO.getPassword());
			person.setUsername(personDTO.getUsername());
			personDAO.create(person);

			return Response.status(Status.CREATED).entity(JsonUtils.getJson(person)).build();
		} catch (Exception ex) {
			LOGGER.error("Failed to create person. Reason: " + ex.getMessage());
			throw new WebApplicationException(
					Response.status(Status.BAD_REQUEST).entity(JsonUtils.getMessageJson(ex.getMessage())).build());
		}
	}

	@GET
	@UnitOfWork
	@Path("/list")
	public Response listPerson(@Auth User user) {
		try {
			List<Person> list = personDAO.findAll();
			return Response.status(Status.OK).entity(JsonUtils.getJson(list)).build();
		} catch (Exception ex) {
			LOGGER.error("Failed to load person list. Reason: " + ex.getMessage());
			throw new WebApplicationException(
					Response.status(Status.BAD_REQUEST).entity(JsonUtils.getMessageJson(ex.getMessage())).build());
		}
	}

}
