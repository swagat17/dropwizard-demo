package com.example.helloworld.resources.dto;

import javax.validation.constraints.NotNull;

public class PersonDTO {

	@NotNull(message = "Full name is mandatory.")
	private String fullName;

	private String jobTitle;

	@NotNull(message = "User name is mandatory.")
	private String username;

	@NotNull(message = "Password is mandatory.")
	private String password;

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
