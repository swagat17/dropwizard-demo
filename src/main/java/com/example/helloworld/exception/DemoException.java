package com.example.helloworld.exception;

public class DemoException extends Exception {

	private static final long serialVersionUID = 999279361546552405L;

	private String message = null;

	public DemoException() {
		super();
	}

	public DemoException(String message) {
		super(message);
		this.message = message;
	}

	public DemoException(Throwable cause) {
		super(cause);
	}

	@Override
	public String toString() {
		return message;
	}

	@Override
	public String getMessage() {
		return message;
	}
}
