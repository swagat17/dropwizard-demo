package com.example.helloworld.db;

import java.util.List;
import java.util.Optional;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import com.example.helloworld.core.Person;

import io.dropwizard.hibernate.AbstractDAO;

public class PersonDAO extends AbstractDAO<Person> {
	public PersonDAO(SessionFactory factory) {
		super(factory);
	}

	public Optional<Person> findById(Long id) {
		return Optional.ofNullable(get(id));
	}

	public Person create(Person person) {
		return persist(person);
	}

	@SuppressWarnings("unchecked")
	public List<Person> findAll() {
		return list(namedQuery("com.example.helloworld.core.Person.findAll"));
	}

	public Person findByUserName(String username) {
		Criteria criteria = criteria();
		criteria.add(Restrictions.eq("username", username));
		criteria.setMaxResults(1);
		return uniqueResult(criteria);
	}

	public Person login(String username, String password) {
		Criteria criteria = criteria();
		criteria.add(Restrictions.eq("username", username));
		criteria.add(Restrictions.eq("password", password));
		criteria.setMaxResults(1);
		return uniqueResult(criteria);
	}
}
